'''
DataDir
author: Nat Egan-Pimblett (nathaniel.ep@gmail.com)
Copyright 2014

Use and share it freely at your own risk.

The object of data-dir is to provide a convenient interface to a collection of
files in a directory (just one-level; flat is better than nested). Given a directory
name, initial filter-by and map-by functions, and a (potentially over-ridden) `load` method,
offers a map-like interface to files and their loaded data.

Possible additions are:
 * Better implementations of the Mapping protocol methods.
 * An implementation of the ContextManager protocol.
'''

import os, re
from collections import Mapping


class DataDir(Mapping):
    '''
    Provide a convenient interface to a directory full of data-files.
    '''

    def __init__(self, dirname, 
                 filter_by=None):
        '''
        Scans `dirname` for files. Optionally filter on filenames by
        providing a callable `filter_by`.
        '''
        self.dirname = dirname
        self.filenames = os.listdir(self.dirname)
        self.filter_by = filter_by
        if filter_by:
            self.filenames = filter(filter_by, self.filenames)

    #Private instance methods (override this for specific file-types)
    def __load__(self, filename, mode="r"):
        return file(os.path.join(self.dirname, filename), mode)

    # Mapping protocol, which gives __contains__, keys, items, values,
    # get, __eq, and __ne__ for "free". 

    # The implementation leans pretty heavily on the vanilla dict for
    # simplicity; could be modified to give better performance for
    # streams, giant files, network acces, etc.
    
    def __getitem__(self, item):
        if item in self.filenames:
            return self.__load__(item)
        else:
            raise KeyError, "Couldn't find {} in {}".format(item, self)

    def __iter__(self):
        return iter(self.filenames)

        
    def __len__(self):
        return len(self.filenames)


    #Public class methods
    @classmethod
    def make_loader(cls, filter_by=None, map_by=None):
        def loader(dirname):
            return cls(dirname, filter_by=filter_by, map_by=map_by)
        return loader
        

    #Public instance methods
    def filter(self, f):
        "Return a dict of those datafiles with for which callable f(name) is True."
        c = type(self)
        if self.filter_by is None:
            return c(self.dirname, filter_by=f)
        else:
            newfilter = lambda fn: f(fn) and self.filter_by(fn)
            return c(self.dirname, filter_by=newfilter)

    def search(self, p):
        "Return a dict of those datafiles whose filenames containthe pattern `p`'"
        return self.filter(lambda f: p in f)
        
    def match(self, p):
        if type(p) == str:
            return self.filter(lambda f: re.search(p, f))
        else:
            #try/except
            return self.filter(lambda f: p.match(f))
