'''
This file contains a class which looks like a dictionary but replaces method
access with a function call (which should be over-ridden by a user).

Copyright 2014 Nathaniel Egan-Pimblett

nathaniel.ep@gmail.com
'''

from itertools import product


class DataBatchError(Exception):
    pass


class DataBatchKeyError(Exception):
    pass


class DataBatch():
    '''
    Collection-shaped container for a processing function and a collection of
    valid arguments for it.

    An example of the strategy pattern.
    '''
    def __init__(self, *keys, **kwargs):
        self.keyindex = keys
        self.keylen = len(self.keyindex)
        if "processor" in kwargs:
            self.procesor = kwargs["processor"]
            

    def __len__(self):
        return reduce(lambda a,b: a*b, map(len, self.keyindex))

    def __getitem__(self, keys):
        if type(keys) != tuple:
            keys = (keys,)
        if not self.keylen == len(keys):
            print repr(keys)
            raise DataBatchError("Expected {} args, received {}.".format(self.keylen, len(keys)))
        for k,ind in zip(keys, self.keyindex):
            if k not in ind:
                raise DataBatchKeyError("{} is not in the index {} of {}".format(k, ind, self))
        return self.__process__(*keys)

    def __iter__(self):
        #naively iterate over all possible key combinations, ignoring
        #ones that aren't valid e.g. invalidated by
        #DataBatchKeyErrors raised  by the user in __getitem__
        for k in product(*self.keyindex):
            try:
                yield self.__process__(*k)
            except DataBatchKeyError:
                continue

    def __contains__(self, item):
        '''Note that this test for a key, not an object, which is a little odd 
        compared to how regular collections work.'''
        return all(map(lambda i, ki: i in ki,
                       item, self.keyindex))

    #This method should be over-ridden to give the "collection" useful
    #behavior.
    def __process__(self, *keys):
        if self.processor:
            self.processor(*keys)
        else:
            raise NotImplementedError("You have to define your own processing method.")
                    

    #This collection is read only, so __setitem__ and __delitem__
    #don't make sense.
    def __setitem__(self, key):
        raise DataBatchError("Can't set an item in a DataBatch.")

    def __delitem__(self, key):
        raise DataBatchError("Can't delete an item in a DataBatch.")
