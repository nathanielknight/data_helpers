'''
ij_tools
author: Nat Egan-Pimblett (nathaniel.ep@gmail.com)

Copyright 2014
Use and share freely at your own risk.

This package contains a collection of functions for workin (with
half-decent performance) with data of the format (i,j,k) where i and j
are indice (e.g. from an array) and k is the value in quesion. It's a
format I've found helpful when interfacing with grid-data when I don't
want to deal with a whole-grid at once (e.g. doing sparse operations).
'''

import os
import numpy as np
import datadir
import itertools as it

#Reading, Writing
def read_ij(filename):
    "Load (i,j,k) data into an nx3 numpy array."
    x = np.fromfile(filename, sep=' ')
    return np.reshape(x, (-1,3))


def write_ij(x, filename):
    "Save an ij_array to file as text"
    np.savetxt(filename, x, delimiter=' ')


def ij_to_dict(ij):
    return {(i,j):k for i,j,k in ij}


def ij_to_array(ij):
    "Convert an ij_array into a max(i) by max(j) array containing the ks."
    ii = max(ij[:,0])
    jj = max(ij[:,1])
    result = np.zeros( (ii, jj) )
    for i,j,k in ij:
        result[i-1,j-1] = k
    return result


def array_to_ij(arr):
    "Convert an array into an ij_array containing the array's elements as ks."
    assert len(arr.shape) == 2, "Expecting a 2-d array; got shape {}".format(arr.shape)
    xlen, ylen = arr.shape
    return np.array([[i+1, j+1, arr[i,j]] for
                     i,j in it.product(range(xlen), range(ylen))])


#Arithmetic
def _arrays_equal(*args):
    'Internal method for comparing an arbitrary number of arrays.'
    ijs = zip(args[:-1], args[1:]) #make pairs of input arrays
    return all(map(lambda ij: np.array_equal(*ij), ijs)) #equality is transitive


def map_ij(f, *args):
    """
    Takes a function of n arguments and n ij_arrays. Checks that the
    indices are the same for all inputs and returns the array
    [(i,j,f(*ks)) for i,j,k in args].
    """
    icols = map(lambda x: x[:,0], args)
    jcols = map(lambda x: x[:,1], args)
    kcols = map(lambda x: x[:,2], args)
    assert _arrays_equal(*icols)
    assert _arrays_equal(*jcols)
    new_k = map(f, *kcols)
    return np.column_stack([icols[0],jcols[0],new_k])


def _add(*args): return sum(args)
def add_ij(*args):
    return map_ij(_add, *args)

def divide_ij(a,b,default=0.0):
    return map_ij(lambda a,b: (a/b) if b!=0 else default, a, b)


#Directory Access
class IJDir(datadir.DataDir):
    def __load__(self, filename):
        return read_ij(os.path.join(self.dirname, filename))
