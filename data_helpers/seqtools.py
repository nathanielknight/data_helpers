"""
Copyright 2014 Nathaniel Egan-Pimblett
nathaniel.ep@gmail.com

Fuck it. We're using lists. This would be better with iterators.
"""

def concat(*args):
    return reduce(lambda a,b: a+b, *args)

def mapcat(f, *args):
    return concat(map(f, *args))

def partition(l, n):
    result = []
    while len(l) >=n:
        x, l = l[:n], l[n:]
        result.append(x)
    if len(l) == 0:
        return result
    else:
        raise UserWarning("Imperfect Partition: had {} elements left over".format(len(l)))
